package pl.jsystems.android.servicesdemo;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements DemoServiceBinder.DownloadCompleteListener {
    private Button btnStart, btnStop, btnIntentservice;
    private DemoServiceBinder binder;
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder = (DemoServiceBinder) service;
            binder.setListener(MainActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            binder.setListener(null);
            binder = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStart = findViewById(R.id.btn_start);
        btnStop = findViewById(R.id.btn_stop);
        btnIntentservice = findViewById(R.id.btn_intentservice);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent =
                        new Intent(MainActivity.this, DemoService.class);
                startService(startIntent);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent =
                        new Intent(MainActivity.this, DemoService.class);
                stopService(startIntent);
            }
        });

        btnIntentservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent =
                        new Intent(MainActivity.this, DemoIntentService.class);
                startIntent.putExtra("name", "Paweł");
                startService(startIntent);
            }
        });

        Intent serviceIntent = new Intent(this, DemoService.class);
        bindService(serviceIntent, connection, Service.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(connection);
    }

    @Override
    public void onDownloadComplete(String file) {
        Toast.makeText(this, "Pobranie zakonczone", Toast.LENGTH_LONG)
                .show();
    }
}
