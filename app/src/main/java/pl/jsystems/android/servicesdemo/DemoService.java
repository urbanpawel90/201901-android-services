package pl.jsystems.android.servicesdemo;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class DemoService extends Service {
    private DemoServiceBinder binder;

    @Override
    public void onCreate() {
        super.onCreate();
        binder = new DemoServiceBinder();
        Log.d("DemoService", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("DemoService", "onStartCommand startId=" + startId);
        new FakeDownloadAsyncTask(binder).execute();
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("DemoService", "onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
