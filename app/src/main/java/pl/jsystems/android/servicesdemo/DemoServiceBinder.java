package pl.jsystems.android.servicesdemo;

import android.os.Binder;

public class DemoServiceBinder extends Binder {
    private DownloadCompleteListener listener;

    public DownloadCompleteListener getListener() {
        return listener;
    }

    public void setListener(DownloadCompleteListener listener) {
        this.listener = listener;
    }

    public interface DownloadCompleteListener {
        void onDownloadComplete(String file);
    }
}
