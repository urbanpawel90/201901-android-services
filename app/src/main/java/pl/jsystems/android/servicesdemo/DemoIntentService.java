package pl.jsystems.android.servicesdemo;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

public class DemoIntentService extends IntentService {
    /*
    Nie możemy zostawić paramatru w konstruktorze po jego wygenerowaniu!
    Trzeba zawsze go usunąć i do super() podać jakąs wartość ręcznie.
     */
    public DemoIntentService() {
        super("DemoIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("DemoIntentService", "onCreate");
        setIntentRedelivery(true);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d("DemoIntentService", "onStartCommand startId=" + startId + ", intent=" + intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("DemoIntentService", "onDestroy");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Log.d("DemoIntentService", "onHandleIntent intent=" + intent);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("DemoIntentService", "END onHandleIntent intent=" + intent);
    }
}
