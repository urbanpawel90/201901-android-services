package pl.jsystems.android.servicesdemo;

import android.os.AsyncTask;
import android.util.Log;

public class FakeDownloadAsyncTask extends AsyncTask<Void, Void, Void> {
    private DemoServiceBinder binder;

    public FakeDownloadAsyncTask(DemoServiceBinder binder) {
        this.binder = binder;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (binder.getListener() != null) {
            binder.getListener().onDownloadComplete("plik.jpg");
        }
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            Thread.sleep(8000);
            Log.d("FakeDownload", "postExecute");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
